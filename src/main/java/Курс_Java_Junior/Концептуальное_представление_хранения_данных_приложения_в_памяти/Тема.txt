Урок 1 - Способы передачи параметров
1. Чему будет равно значение переменной a в результате выполнения следующего блока кода?
187
2. Загрузи исходный код метода findDebitum()
private static void findDebitum(int cntServices, int cntDebtors, double[] services, double[][] debtors, double[] result) {
    for (int j = 0; j < cntDebtors; ++j) {
        double sum = 0;
        for (int k = 0; k < cntServices; ++k) {
            sum += services[k] * debtors[k][j];
        }
        result[j] = sum;
    }
}