package Курс_Java_Junior.Исключения;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

// Класс для перевода данных из текстового файла в список
public class FileToArray {
    // Имя файла
    private final String fileName = "Notes.txt";

    // Получить список строк файла
    public List<String> readArray() {
        // Создаём список для строк файла
        List<String> result = new ArrayList<>();
        try {
            // Создаём объект fileReader, а затем на его основе объект BufferedReader для построчного чтения файла
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            // Пока файл не пуст, читаем его построчно, каждую строку добавляем в список
            while (true) {
                String line = bufferedReader.readLine();
                // Если дошли до конца файла - выходим из цикла
                if (line == null)
                    break;
                result.add(line);
            }
            // После работы закрываем файл
            fileReader.close();
        }
        catch (FileNotFoundException e) {
            System.out.println("Файл не найден");
        }
        catch (IOException e) {
            System.out.println("Ошибка");
        }
        finally {
            System.out.println("Работа с файлом окончена");
        }
        return result;
    }

    // Добавить в файл строки
    public void writeIntoFile() {
        // Создаём список для строк, считываемых из консоли
        List<String> input = new ArrayList<>();
        System.out.println("Вводите строки, которые хотите добавить в файл. Признак окончания ввода - пустая строка");
        // Для считывания строк используем Scanner
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if (line == null || line.length() == 0)
                break;
            input.add(line);
        }
        sc.close();
        try {
            // Для записи файла используем FileWriter. Записываем строки, после каждой добавляя независимый
            // от платформы перевод строки. Файл дозаписываем, а не очищаем его
            FileWriter writer = new FileWriter(fileName, true);
            for (var line : input) {
                writer.write(line);
                writer.append(String.format("%n"));
            }
            writer.close();
        }
        catch (IOException e) {
            System.out.println("Ошибка записи в файл");
        }
        finally {
            System.out.println("Работа с файлом окончена");
        }
    }
}
