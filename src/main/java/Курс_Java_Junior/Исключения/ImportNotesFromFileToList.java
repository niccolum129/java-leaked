package Курс_Java_Junior.Исключения;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ImportNotesFromFileToList {

    public static List<String> readNotesToList(String pathToNotes) {
        List<String> importedNotes = new ArrayList<>();

        try (FileReader fr = new FileReader(pathToNotes);
             BufferedReader br = new BufferedReader(fr)) {

            String line;
            while ( (line = br.readLine()) != null ) {
                line = line.trim();
                if (!line.isEmpty()) {
                    importedNotes.add(line);
                }
            }

        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Unknown error");
        } finally {
            System.out.println("Process finished");
        }

        return importedNotes;
    }

    public static void writeCommentToNotes(String pathToNotes) {
        List<String> comments = new ArrayList<>();

        System.out.println("Введите комментарии. Выход - пустая строка");

        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if (line == null || line.trim().isEmpty())
                break;
            comments.add(line);
        }

        try (FileWriter fw = new FileWriter(pathToNotes);
             BufferedWriter bw = new BufferedWriter(fw)) {

            for (String comment : comments) {
                bw.write("#");
                bw.write(comment);
                bw.newLine();
            }

        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Unknown error");
        } finally {
            System.out.println("Process finished");
        }
    }
}
