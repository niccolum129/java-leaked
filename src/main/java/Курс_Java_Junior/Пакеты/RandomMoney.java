package Курс_Java_Junior.Пакеты;

package com.intellekta.randommoney;

import java.util.Random;

public class RandomMoney {
    private double price;

    public double getPrice() {
        return price;
    }

    public void sale(int cnt, double time) {
        this.price = 5 * Math.sin(0.5 * time);
        double result = this.price * cnt;

        if (result > 0) {
            System.out.printf("The company has earned %.2f USD", result);
        } else if (result == 0) {
            System.out.print("The company did not earn anything and did not lose on the sale");
        } else {
            System.out.printf("The company has lost %.2f USD", -result);
        }
    }

    public void testSale() {
        Random random = new Random();
        sale(random.nextInt(), random.nextDouble() * 50);
        sale(random.nextInt(), random.nextDouble() * 50);
        sale(random.nextInt(), random.nextDouble() * 50);
    }
}
