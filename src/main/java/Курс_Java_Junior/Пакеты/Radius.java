package Курс_Java_Junior.Пакеты;

package mypackage;

public class Radius {
    public static double getSquare(int radius) {
        return radius < 0 ?
                -1 :
                Math.PI * Math.pow(radius, 2);
    }
}
