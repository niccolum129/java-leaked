package Курс_Java_Junior.Пакеты;

package testpackage;

import static mypackage.Radius.getSquare;

public class Task {
    public static double task(int radius) {
        return getSquare(radius);
    }
}
