package Курс_Java_Junior.Реализация_алгоритмов_сортировки;

// Алгоритм сортировки пузырьком

public class BubbleSort {
    /**
     * Метод для синхронной сортировки двух массивов данных. Перед использованием, массивы данных уже должны быть
     * синхронизированы! Используется алгоритм сортировки пузырьком. Если длина массивов не совпадает, в консоль будет
     * выведено сообщение "Corrupted Data", после чего метод завершит свою работу. Если длина массивов равна 0,
     * в консоль будет выведено сообщение "Empty Data", после чего метод завершит свою работу. После каждой перестановки
     * в консоль выводится состояние массивов, с помощью метода printData. После окончания сортировки в консоль выводится
     * состояние массивов, с помощью метода printData.
     * @param countries Строковый массив, содержит названия стран
     * @param sales Массив вещественных чисел, содержит данные о продажах
     */
    public static void sortSales(String[] countries, double[] sales) {
        // если длина массивов не совпадает, пишем, что данные закораптились
        if (countries.length != sales.length) {
            System.out.println("Corrupted Data");
            return;
        }
        // иначе, если у обоих массивов нулевая длина, то пишем, что пусто
        if (countries.length == 0) {
            System.out.println("Empty Data");
            return;
        }
        // если все хорошо, сортируем пузырьком
        for(int i = 1; i < sales.length; ++i) {
            for(int j = 0; j < sales.length - i; ++j) {
                if (sales[j] < sales[j + 1]) {
                    // для синхронных перестановок используем метод doubleSwap
                    doubleSwap(countries, sales, j, j+1);
                    // после чего показываем перестановку
                    printData(countries, sales);
                }
            }
        }
        // в конце показываем результат
        printData(countries, sales);
    }

    /**
     * Метод для сихронных перестановок в двух массивах. Используется в методе sortSales.
     * @param countries Строковый массив, содержит названия стран
     * @param sales Массив вещественных чисел, содержит данные о продажах
     * @param idx1 Индекс первого элемента для перестановки
     * @param idx2 Индекс второго элемента для перестановки
     */
    private static void doubleSwap(String[] countries, double[] sales, int idx1, int idx2) {
        // сохраняем значения под idx1 во временные переменные
        String tmpCountry = countries[idx1];
        double tmpSale = sales[idx1];

        // меняем элементы:
        countries[idx1] = countries[idx2];
        sales[idx1] = sales[idx2];

        countries[idx2] = tmpCountry;
        sales[idx2] = tmpSale;
    }






    private static void printData(String[] names, double[] data) {
        System.out.print("{");
        for (int i=0; i<data.length;i++){
            System.out.print(names[i]+": "+data[i]+", ");
        }
        System.out.println("\b\b}");
    }
}
