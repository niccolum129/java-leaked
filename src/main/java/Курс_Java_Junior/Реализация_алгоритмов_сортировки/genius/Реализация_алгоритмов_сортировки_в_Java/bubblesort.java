package com.javacourse;

import java.util.Objects;

public class Main {
	private static void printData(String[] names, double[] data) {
		System.out.print("{");
		for (int i=0; i<data.length;i++){
			System.out.print(names[i]+": "+data[i]+", ");
		}
		System.out.println("\b\b}");
	}

	public static void sortSales(String[] countries, double[] sales) {
		// Проверяем, не повреждены ли данные (размеры массивов отличаются?)
		if (countries.length != sales.length) {
			System.out.println("Corrupted Data");
			return;
		}
		// Проверяем, присутствуют ли данные (массивы не пусты?)
		if (countries.length == 0 && sales.length == 0) {
			System.out.println("Empty Data");
			return;
		}
		// Выполняем сортировку
		int dataLength = sales.length;
		// Сортировка проходит за (dataLength - 1) шаг, на каждом шаге встаёт на место один элемент
		for (int step = 1; step < dataLength; step++) {
			// Последовательно проходим по массиву и выполняем сравнения
			for (int i = 0; i < dataLength - step; i++) {
				// Если текущий элемент меньше следующего, меняем их местами
				if (sales[i] < sales[i + 1]) {
					// Запоминаем следующий элемент
					double saleTmp = sales[i + 1];
					String countryTmp = countries[i + 1];
					// Заменяем следующий элемент текущим
					sales[i + 1] = sales[i];
					countries[i + 1] = countries[i];
					// Запомненное значение следующего элемента ставим на место текущего
					sales[i] = saleTmp;
					countries[i] = countryTmp;
					// Печатаем содержимое массивов
					printData(countries, sales);
				}
			}
		}
		// Выводим окончательный результат
		printData(countries, sales);
	}

	public static void main(String[] args) {
		String[] countries = {"Russia", "UK", "Germany", "USA", "Denmark"};
		double[] sales = {55, 40, 80, 33, 34};
		sortSales(countries, sales);
	}
}