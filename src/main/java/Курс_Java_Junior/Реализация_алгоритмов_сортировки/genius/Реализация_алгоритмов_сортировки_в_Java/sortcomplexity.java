package com.javacourse;

import java.util.Objects;
import java.util.Arrays;

public class Main {
	private static void printData(String[] names, double[] data) {
		System.out.print("{");
		for (int i=0; i<data.length;i++){
			System.out.print(names[i]+": "+data[i]+", ");
		}
		System.out.println("\b\b}");
	}

	public static void qSortSales(String[] unitNames, double[] sales, int leftBorder, int rightBorder) {
		// Определяем левый и правый маркеры, которые будем двигать
		int leftMarker = leftBorder;
		int rightMarker = rightBorder;
		// Вычисляем барьерный элемент
		double pivot = sales[(leftMarker + rightMarker) / 2];
		do {
			// Двигаем левый маркер слева направо пока элемент меньше, чем pivot
			while (sales[leftMarker] < pivot)
				leftMarker++;
			// Двигаем правый маркер, пока элемент больше, чем pivot
			while (sales[rightMarker] > pivot)
				rightMarker--;
			// Проверим, не нужно ли обменять местами элементы, на которые указывают маркеры
			if (leftMarker <= rightMarker) {
				// Если левый маркер меньше правого, то выполняем обмен их элементов
				if (leftMarker < rightMarker) {
					double tmpSale = sales[leftMarker];
					String tmpUnit = unitNames[leftMarker];
					sales[leftMarker] = sales[rightMarker];
					unitNames[leftMarker] = unitNames[rightMarker];
					sales[rightMarker] = tmpSale;
					unitNames[rightMarker] = tmpUnit;
				}
				// Сдвигаем маркеры, чтобы получить новые границы
				leftMarker++;
				rightMarker--;
			}
		} while (leftMarker <= rightMarker);
		// Выполняем рекурсивно для частей
		if (leftMarker < rightBorder)
			qSortSales(unitNames, sales, leftMarker, rightBorder);
		if (leftBorder < rightMarker)
			qSortSales(unitNames, sales, leftBorder, rightMarker);
	}

	public static void sortSales(String[] unitNames, double[] sales) {
		// Проверяем, не повреждены ли данные (размеры массивов отличаются?)
		if (unitNames.length != sales.length) {
			System.out.println("Corrupted Data");
			return;
		}
		// Проверяем, присутствуют ли данные (массивы не пусты?)
		if (unitNames.length == 0 && sales.length == 0) {
			System.out.println("Empty Data");
			return;
		}
		// Для сортировки используем рекурсивную реализацию быстрой сортировки
		qSortSales(unitNames, sales, 0, unitNames.length - 1);
		// Выводим окончательный результат
		printData(unitNames, sales);
	}

	public static void main(String[] args) {
		String[] countries = {"Russia", "UK", "Germany", "USA", "Denmark"};
		double[] sales = {55, 40, 80, 33, 34};
		String[] countries1 = {"Russia", "UK", "Germany", "USA"};
		double[] sales1 = {55, 40, 80, 33};
		sortSales(countries, sales);
	}
}