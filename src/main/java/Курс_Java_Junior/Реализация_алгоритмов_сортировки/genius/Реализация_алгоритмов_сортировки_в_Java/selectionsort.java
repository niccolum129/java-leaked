package com.javacourse;

import java.util.Objects;

public class Main {
	public static void sortSales(String[] countries, double[] sales) {
		// Проверяем, не повреждены ли данные (размеры массивов отличаются?)
		if (countries.length != sales.length) {
			System.out.println("Corrupted Data");
			return;
		}
		// Проверяем, присутствуют ли данные (массивы не пусты?)
		if (countries.length == 0 && sales.length == 0) {
			System.out.println("Empty Data");
			return;
		}
		// Выполняем сортировку
		int dataLength = sales.length;
		// Сортировка проходит за (dataLength / 2) шагов, на каждом шаге встают на место два элемента
		for (int step = 1; step <= dataLength / 2; step++) {
			// Объявляем переменные для хранения максимума и минимума.
			// Сначала считаем, что максимум и минимум - это первый неотсортированный элемент с индексом (step - 1).
			// * -1, т.к. смещение. Шаг первый, а первый неотсортированный элемент - нулевой
			int maxId, minId;
			maxId = minId = step - 1;
			double max, min;
			max = min = sales[step - 1];
			// Ищем среди неотсортированных элементов максимум и минимум
			for (int i = step; i <= dataLength - step; i++) {
				if (sales[i] > max) {
					max = sales[i];
					maxId = i;
				}
				if (sales[i] < min) {
					min = sales[i];
					minId = i;
				}
			}
			String countryTmp;
			// Найденный максимум ставим в конец неотсортированной части массива
			sales[maxId] = sales[dataLength - step];
			sales[dataLength - step] = max;
			// Соответствующим образом делаем перестановку в массиве стран
			countryTmp = countries[maxId];
			countries[maxId] = countries[dataLength - step];
			countries[dataLength - step] = countryTmp;
			// Мы совершили перестановку в массиве. А значит, могли переставить минимальный элемент, если он был
			// последним в неотсортированной части, по индексу (dataLength - step)
			// Если это так, то меняем индекс минимального элемента на новый его индекс
			if (dataLength - step == minId)
				minId = maxId;

			// Найденный минимум ставим в начало неотсортированной части массива
			sales[minId] = sales[step - 1];
			sales[step - 1] = min;
			// Соответствующим образом делаем перестановку в массиве стран
			countryTmp = countries[minId];
			countries[minId] = countries[step - 1];
			countries[step - 1] = countryTmp;

			// Печатаем массивы после перестановки
			printData(countries, sales);
		}
		// Выводим окончательный результат
		printData(countries, sales);
	}

	public static void main(String[] args) {
		String[] countries = {"Russia", "UK", "Germany", "USA", "Denmark"};
		double[] sales = {55, 40, 80, 33, 34};
		String[] countries1 = {"Russia", "UK", "Germany", "USA"};
		double[] sales1 = {55, 40, 80, 33};
		sortSales(countries1, sales1);
	}
}