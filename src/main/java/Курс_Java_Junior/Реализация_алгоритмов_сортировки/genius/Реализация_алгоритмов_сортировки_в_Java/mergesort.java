package com.javacourse;

import java.util.Objects;

public class Main {
	private static void printData(String[] names, double[] data) {
		System.out.print("{");
		for (int i=0; i<data.length;i++){
			System.out.print(names[i]+": "+data[i]+", ");
		}
		System.out.println("\b\b}");
	}

	private static void mergeSort(String[] stores, double[] sales) {
		int arraysLength = stores.length; // длина stores и sales одинакова
		// Вычисляем длину будущих левой и правой частей массивов
		int leftArraysLength = arraysLength / 2;
		int rightArraysLength = arraysLength - leftArraysLength;
		// Создаём левые и правые кусочки массивов
		String[] leftStores = new String[leftArraysLength];
		double[] leftSales = new double[leftArraysLength];
		String[] rightStores = new String[rightArraysLength];
		double[] rightSales = new double[rightArraysLength];
		// Заполняем созданные кусочки
		for (int i = 0; i < arraysLength; i++) {
			if (i < leftArraysLength) {
				leftStores[i] = stores[i];
				leftSales[i] = sales[i];
			}
			else {
				rightStores[i - leftArraysLength] = stores[i];
				rightSales[i - leftArraysLength] = sales[i];
			}
		}
		// Если нужно, дробим массивы ещё, и сортируем их
		if (rightArraysLength > 1) {
			mergeSort(leftStores, leftSales);
			mergeSort(rightStores, rightSales);
		}
		// Сливаем отсортированные левые и правые части массивов
		int leftId = 0;
		int rightId = 0;
		for (int i = 0; i < arraysLength; i++) {
			boolean isLeftEnd = leftId == leftArraysLength; // левая часть массива просмотрена?
			boolean isRightEnd = rightId == rightArraysLength; // правая часть массива просмотрена?
			// Идём по массиву и располагаем элементы отсортированных левого и правого кусочков в правильном порядке
			if (isLeftEnd && !isRightEnd || !isLeftEnd && !isRightEnd && leftSales[leftId] > rightSales[rightId]) {
				stores[i] = rightStores[rightId];
				sales[i] = rightSales[rightId];
				rightId++;
			}
			else if (!isLeftEnd && isRightEnd || !isLeftEnd && !isRightEnd && leftSales[leftId] <= rightSales[rightId]) {
				stores[i] = leftStores[leftId];
				sales[i] = leftSales[leftId];
				leftId++;
			}
		}
		// Выводим результат сортировки массива на этом уровне
		printData(stores, sales);
	}

	public static void sortStoresSales(String[] stores, double[] sales) {
		// Проверяем, не повреждены ли данные (размеры массивов отличаются?)
		if (stores.length != sales.length) {
			System.out.println("Corrupted Data");
			return;
		}
		// Проверяем, присутствуют ли данные (массивы не пусты?)
		if (stores.length == 0 && sales.length == 0) {
			System.out.println("Empty Data");
			return;
		}
		// Выполняем сортировку
		mergeSort(stores, sales);
	}

	public static void main(String[] args) {
		String[] countries = {"Russia", "UK", "Germany", "USA", "Denmark"};
		double[] sales = {55, 40, 80, 33, 34};
		String[] countries1 = {"Russia", "UK", "Germany", "USA"};
		double[] sales1 = {55, 40, 80, 33};
		sortStoresSales(countries, sales);
	}
}