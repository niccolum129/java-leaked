package Курс_Java_Junior.Реализация_алгоритмов_сортировки;

// Алгоритм сортировки выбором

public class ChoiceSort {
    /**
     * Метод для синхронной сортировки двух массивов данных по возрастанию. Перед использованием, массивы данных уже
     * должны быть синхронизированы! Используется алгоритм сортировки двунаправленным выбором. Если длина массивов не
     * совпадает, в консоль будет выведено сообщение "Corrupted Data", после чего метод завершит свою работу. Если
     * длина массивов равна 0, в консоль будет выведено сообщение "Empty Data", после чего метод завершит свою работу.
     * После каждой двунаправленной перестановки в консоль выводится состояние массивов, с помощью метода printData.
     * После окончания сортировки в консоль выводится состояние массивов, с помощью метода printData.
     * @param countries Строковый массив, содержит названия стран
     * @param sales Массив вещественных чисел, содержит данные о продажах
     */
    public static void sortSales(String[] countries, double[] sales) {
        // если длина массивов не совпадает, пишем, что данные закораптились
        if (countries.length != sales.length) {
            System.out.println("Corrupted Data");
            return;
        }
        // иначе, если у обоих массивов нулевая длина, то пишем, что пусто
        if (countries.length == 0) {
            System.out.println("Empty Data");
            return;
        }
        // если все хорошо, сортируем двунапавленным выбором:
        // для двунаправленности определим k - индекс конца неотсортированной части массива
        // индекс i - начало неотсортированной части массива
        int k = sales.length;
        for (int i = 0; i < sales.length; ++i) {
            // апдейтим индексы максимума и минимума для текущей итерации
            // берем для минимального начало, а для максимального конец неотсортированной части
            int maxIdx = --k;
            int minIdx = i;
            // если индексы встретились, значит обе части массива отсортированы, останавливаем сортировку
            if (k <= i) {
                break;
            }

            double minVal = sales[minIdx];
            double maxVal = sales[maxIdx];
            // в границах неотсортированного массива ищем minIdx и maxIdx
            for (int j = i; j <= k; ++j) {
                if (sales[j] < minVal) {
                    minVal = sales[j];
                    minIdx = j;
                }
                if (sales[j] > maxVal) {
                    maxVal = sales[j];
                    maxIdx = j;
                }
            }
            // если индекс менялся, синхронно меняем элементы с индексами minIdx и i
            if (minIdx != i) {
                doubleSwap(countries, sales, minIdx, i);
                if (maxIdx == i) {
                    maxIdx = minIdx;
                }
            }
            // если индекс менялся, синхронно меняем элементы с индексами maxIdx и k
            if (maxIdx != k) {
                doubleSwap(countries, sales, maxIdx, k);
            }
            // после перестановок показываем состояние
            printData(countries, sales);
        }

        // в конце показываем результат
        printData(countries, sales);
    }

    /**
     * Метод для сихронных перестановок в двух массивах. Используется в методе sortSales.
     * @param countries Строковый массив, содержит названия стран
     * @param sales Массив вещественных чисел, содержит данные о продажах
     * @param idx1 Индекс первого элемента для перестановки
     * @param idx2 Индекс второго элемента для перестановки
     */
    private static void doubleSwap(String[] countries, double[] sales, int idx1, int idx2) {
        // сохраняем значения под idx1 во временные переменные
        String tmpCountry = countries[idx1];
        double tmpSale = sales[idx1];

        // меняем элементы:
        countries[idx1] = countries[idx2];
        sales[idx1] = sales[idx2];

        countries[idx2] = tmpCountry;
        sales[idx2] = tmpSale;
    }






    private static void printData(String[] names, double[] data) {
        System.out.print("{");
        for (int i=0; i<data.length;i++){
            System.out.print(names[i]+": "+data[i]+", ");
        }
        System.out.println("\b\b}");
    }
}
