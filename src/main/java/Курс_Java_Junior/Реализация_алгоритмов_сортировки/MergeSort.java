package Курс_Java_Junior.Реализация_алгоритмов_сортировки;

// Алгоритм сортировки слиянием

public class MergeSort {
    /**
     * Метод для синхронной сортировки двух массивов данных по возрастанию. Перед использованием, массивы данных уже
     * должны быть синхронизированы! Используется рекурсивный алгоритм сортировки слиянием. Если длина массивов не
     * совпадает, в консоль будет выведено сообщение "Corrupted Data", после чего метод завершит свою работу. Если
     * длина массивов равна 0, в консоль будет выведено сообщение "Empty Data", после чего метод завершит свою работу.
     * После каждого слияния в консоль выводится состояние массивов, с помощью метода printData.
     * @param stores Строковый массив, содержит названия магазинов
     * @param sales Массив вещественных чисел, содержит данные о продажах
     */
    public static void sortStoreSales(String[] stores, double[] sales) {
        // если длина массивов не совпадает, пишем, что данные закораптились
        if (stores.length != sales.length) {
            System.out.println("Corrupted Data");
            return;
        }
        // иначе, если у обоих массивов нулевая длина, то пишем, что пусто
        if (stores.length == 0) {
            System.out.println("Empty Data");
            return;
        }
        // если все хорошо, сортируем слиянием
        mergeSort(stores, sales);
    }

    /**
     * Метод синхронной сортировки двух массивов алгоритмом слияния по возрастанию. После каждого слияния метод выводит
     * в консоль состояние массивов с помощью метода printData. Данный метод используется в методе sortStoreSales.
     * @param stores Строковый массив, содержит названия магазинов
     * @param sales Массив вещественных чисел, содержит данные о продажах
     */
    private static void mergeSort(String[] stores, double[] sales) {
        // создаем левые и правые массивы данных
        double[] leftSales = new double[sales.length / 2];
        double[] rightSales = new double[sales.length - leftSales.length];
        String[] leftStores = new String[leftSales.length];
        String[] rightStores = new String[rightSales.length];

        // заполняем
        for (int i = 0; i < sales.length; i++) {
            if (i < leftSales.length) {
                leftSales[i] = sales[i];
                leftStores[i] = stores[i];
            } else {
                rightSales[i - leftSales.length] = sales[i];
                rightStores[i - leftSales.length] = stores[i];
            }
        }

        // дробим
        if (rightSales.length > 1) {
            mergeSort(leftStores, leftSales);
            mergeSort(rightStores, rightSales);
        }

        // сливаем и сортируем
        int leftIdx = 0;
        int rightIdx = 0;
        for (int i = 0; i < sales.length; i++) {
            boolean isLeftEnded = leftIdx == leftSales.length;
            boolean isRightEnded = rightIdx == rightSales.length;

            if (isLeftEnded && !isRightEnded || !isRightEnded && leftSales[leftIdx] > rightSales[rightIdx]) {
                stores[i] = rightStores[rightIdx];
                sales[i] = rightSales[rightIdx++];
            }
            else if (!isLeftEnded && isRightEnded || !isLeftEnded && leftSales[leftIdx] <= rightSales[rightIdx])
            {
                stores[i] = leftStores[leftIdx];
                sales[i] = leftSales[leftIdx++];
            }

        }
        // выводим результат
        printData(stores, sales);
    }






    private static void printData(String[] names, double[] data) {
        System.out.print("{");
        for (int i=0; i<data.length;i++){
            System.out.print(names[i]+": "+data[i]+", ");
        }
        System.out.println("\b\b}");
    }
}
