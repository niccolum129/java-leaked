package Курс_Java_Junior.Класс_Arrays;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Task {
    public static double[] findAllWinners(double[] hours) {
        // если массив не существует или пустой, то возвращаем пустой массив
        if (hours == null || hours.length == 0) {
            return new double[0];
        }
        // отсортируем все часы по возрастанию
        Arrays.sort(hours);
        // заведем счетчик групп с одинаковым числом часов, поскольку массив отсортирован
        // все числа одной группы расположены рядом
        // на данном этапе в массиве есть как минимум 1 элемент, образующий свою группу, поэтому
        // счетчик сразу на 1
        int uniqueGroupCount = 1;
        int idx;
        // теперь надо с конца массива выделить 3 группы (3 степени награды), т.е. будем
        // считать найденные группы и найдя 4-ую, обнаружим 3 группы + 1 число часов из 4-ой группы
        // также учитываем, что можем найти начало массива раньше, чем найдем 3 группы
        for (idx = hours.length - 1; idx >= 1 && uniqueGroupCount < 4; idx--) {
            if (Array.getDouble(hours, idx) != Array.getDouble(hours, idx - 1)) {
                uniqueGroupCount++;
            }
        }
        // далее, если было найдено 4 группы, увеличим индекс последнего числа 4 группы на 1,
        // чтобы получить индекс начала 3 группы и скопируем отсюда и до конца в массив - это результат
        // если не было найдено 4 группы, значит мы дошли до начала исходного массива
        if (uniqueGroupCount == 4)
            idx++;
        return Arrays.copyOfRange(hours, idx, hours.length);
    }

    public static int peopleChoiceAward(double[] hours, int count) {
        // если массив не существует или пустой, возвращаем -1
        if (hours == null || hours.length == 0 || count <= 0) {
            return -1;
        }
        // отсортируем для бнарного поиска
        Arrays.sort(hours);
        // попробуем найти индекс элемента равного 1500, в пределах:
        // от начала массива, до начала групп победителей другого конкурса (0, length - winners count)
        // поскольку вторая граница невключается, -1 не надо
        int idx = Arrays.binarySearch(hours, 0, hours.length - count, 1500);
        return Math.max(-1, idx); // вернем индекс или -1
    }
}
