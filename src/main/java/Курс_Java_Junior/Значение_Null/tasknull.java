package com.javacourse;

public class Main {
	public static void printDataArray(int[][] data) {
		for (int[] elem : data) {
			System.out.print("{ ");
			for (int attr : elem) {
				System.out.print(attr + " ");
			}
			System.out.println(" }");
		}
	}

	public static void sort(int[][] data) {
		// Проверяем данные на правильность
		// Есть ли данные?
		if (data == null || data.length == 0) {
			System.out.println("Empty data");
			return;
		}
		int dataLength = data.length;
		// Корректны ли данные о пользователях?
		for (int i = 0; i < dataLength; i++) {
			if (data[i] == null) {
				System.out.println("Empty data at index " + i);
				return;
			}
			if (data[i].length != 3) {
				System.out.println("Unavailable data at index " + i);
				return;
			}
		}
		// Сортировка пузырьком по убыванию
		for (int step = 1; step < dataLength; step++) {
			for (int i = 0; i < dataLength - step; i++) {
				if (data[i][0] < data[i+1][0]) {
					var tmp = data[i];
					data[i] = data[i + 1];
					data[i + 1] = tmp;
				}
			}
		}
	}

	public static void main(String[] args) {
		int[][] array = {{0,0,2}, {1,1,5}, {5,1,5}, {3,1,5}};
		sort(array);
		printDataArray(array);
	}
}