package com.intellekta.staff;

// Класс работника с почасовой зарплатой
public class HourlyStaff implements Staff {
	// Количество отработанных в сутки часов
	private int workTime;
	public int getWorkTime() { return workTime; }

	// Конструктор
	public HourlyStaff(int workTime) {
		this.workTime = workTime >= 4 && workTime <= 10 ? workTime : 0;
	}

	// Рассчитать зарплату
	@Override
	public int calculateSalary(int salary) {
		// Проверяем правильность значения зарплаты
		if (salary < 0)
			salary = 0;
		return salary * workTime;
	}
}