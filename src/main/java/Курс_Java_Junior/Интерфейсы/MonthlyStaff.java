package com.intellekta.staff;

// Класс работника с месячной зарплатой
public class MonthlyStaff implements Staff {
	// Количество отработанных дней
	private int workDays;
	public int getWorkDays() { return workDays; }
	// Премия
	private int premium;
	public int getPremium() { return premium; }

	// Конструктор
	public MonthlyStaff(int workDays, int premium) {
		this.workDays = (workDays <= 0 || workDays > 30) ? 0 : workDays;
		this.premium = (premium < 0 || premium > 10000) ? 0 : premium;
	}

	// Рассчитать зарплату
	@Override
	public int calculateSalary(int salary) {
		// Проверяем правильность значения зарплаты
		if (salary < 0)
			salary = 0;
		return salary * workDays + premium;
	}
}