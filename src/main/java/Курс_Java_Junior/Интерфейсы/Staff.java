package com.intellekta.staff;

// Интерфейс работника
public interface Staff {
	// Рассчитать зарплату
	int calculateSalary(int salary);
}