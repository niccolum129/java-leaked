package com.intellekta.staff;

// Класс работника с недельной зарплатой
public class WeeklyStaff extends MonthlyStaff implements Staff {
	// Количество рабочих недель
	private int workWeeks;
	public int getWorkWeeks() { return workWeeks; }

	// Конструктор
	public WeeklyStaff(int workDays, int premium, int workWeeks) {
		super(workDays, premium);
		this.workWeeks = (workWeeks >= 2 && workWeeks <= 4) ? workWeeks : 0;
	}

	// Рассчитать зарплату
	@Override
	public int calculateSalary(int salary) {
		// Проверяем правильность значения зарплаты
		if (salary < 0)
			salary = 0;
		return workWeeks * salary;
	}

	// Печать годовой зарплаты работника
	public void printYearSalary(int salary) {
		final int yearWeeksCount = 26; // кол-во недель в году: 52. Из них половина рабочих
		System.out.println(calculateSalary(salary) * yearWeeksCount);
	}
}