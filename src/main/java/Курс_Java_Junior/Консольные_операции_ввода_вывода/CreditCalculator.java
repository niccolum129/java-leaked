package Курс_Java_Junior.Консольные_операции_ввода_вывода;

package com.intellekta.creditbank;

import java.util.Scanner;

public class CreditCalculator {
    String lastName;
    String firstName;
    String secondName;
    double sum;
    int loanMaturity;
    double interestRate;

    public CreditCalculator() {
        Scanner scanner = new Scanner(System.in);

        this.lastName = askString(
                "Lastname",
                "Lastname is incorrect. It must be not empty string",
                scanner);
        this.firstName = askString(
                "Firstname",
                "Firstname is incorrect. It must be not empty string",
                scanner);

        System.out.print("Secondname: ");
        this.secondName = scanner.nextLine().trim();

        this.interestRate = askDouble(
                "Interest rate",
                "Interest rate is incorrect. It must be strictly positive number",
                scanner);
        this.sum = askDouble(
                "Credit amount",
                "Credit amount is incorrect. It must be strictly positive number",
                scanner);

        this.loanMaturity = askInteger(
                "Loan maturity",
                "Loan maturity is incorrect. It must be strictly positive int",
                scanner);
    }

    // метод для ввода строки
    // запрашивает у пользователя строку в виде "field: "
    // при пустой строке напишет warning и начнет сначала
    private String askString(String field, String warning, Scanner scanner) {
        System.out.printf("%s: ", field);
        String str = scanner.nextLine();

        while (str.trim().isEmpty()) {
            System.out.println(warning);
            System.out.printf("%s: ", field);
            str = scanner.nextLine();
        }

        return str;
    }

    // метод для ввода действительных чисел
    // запрашивает у пользователя число в виде "field: "
    // при не-числе или при неположительном числе напишет warning и начнет сначала
    private double askDouble(String field, String warning, Scanner scanner) {
        System.out.printf("%s: ", field);
        double val = 0;
        boolean incorrectInput = false;

        do {
            if (incorrectInput) {
                System.out.println(warning);
                System.out.printf("%s: ", field);
                scanner.nextLine();
            }
            if (scanner.hasNextDouble()) {
                val = scanner.nextDouble();
                incorrectInput = val <= 0;
            } else {
                incorrectInput = true;
            }

        } while (incorrectInput);
        scanner.nextLine();

        return val;
    }

    // метод для ввода целых чисел
    // запрашивает у пользователя число в виде "field: "
    // при не-числе или при неположительном числе напишет warning и начнет сначала
    private int askInteger(String field, String warning, Scanner scanner) {
        System.out.printf("%s: ", field);
        int val = 0;
        boolean incorrectInput = false;

        do {
            if (incorrectInput) {
                System.out.println(warning);
                System.out.printf("%s: ", field);
                scanner.nextLine();
            }
            if (scanner.hasNextInt()) {
                val = scanner.nextInt();
                incorrectInput = val <= 0;
            } else {
                incorrectInput = true;
            }

        } while (incorrectInput);
        scanner.nextLine();

        return val;
    }

    // посчитает сумму ежемесячного платежа для клиента в виде действительного числа с точностью до копеек
    public void printCreditAgreement() {
        double t = interestRate / 12 / 100;
        double pl = sum * t / (1 - Math.pow(1 + t, -loanMaturity));
        System.out.printf("%.2f%n", pl);
    }
}
