package com.javacourse;

import java.util.Objects;

public class Main {
	public static void checkWeight(double[] weights) {
		// Проверяем, а не пуст ли массив
		if (weights.length == 0) {
			System.out.println("Not enough substance");
			return;
		}
		final double standardWeight = 0.050; // эталонный вес
		int tabletsCount = weights.length;
		boolean isOk = true; // Есть ли бракованные таблетки? Пока считаем, что все таблетки имеют нужный вес
		// Проверяем вес каждой третьей таблетки
		for (int i = 0; isOk && i < tabletsCount; i += 3)
			// Вычисляем процент, который составляет вес таблетки weight от standardWeight. Он должен быть более 90.
			// Если нет - мы нашли брак, дальше проверять не нужно, выходим из цикла по условию
			if (weights[i] / (standardWeight / 100) <= 90)
				isOk = false;
		// Если есть хотя бы одна бракованная таблетка, говорим об этом. Иначе - Ok
		if (isOk)
			System.out.println("Ok");
		else
			System.out.println("Not enough substance");
	}

	public static void main(String[] args) {
		double[] tabletsWeights = {0.02, 0.048, 0.052, 0.037, 0.050, 0.048, 0.049, 0.054, 0.048, 0.045};
		checkWeight(tabletsWeights);
	}
}