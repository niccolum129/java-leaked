package com.javacourse;

import java.util.Objects;

public class Main {
	public static void sayHello(String[] args) {
		// Получаем от системы имя текущего пользователя
		String username = System.getProperty("user.name");
		// Если параметр "-u", выводим в приветствии имя пользователя. Иначе - просто "Hello!"
		String welcomeText = (args.length == 1 && args[0].equals("-u")) ? "Hello, " + username + "!" : "Hello!";
		System.out.println(welcomeText);
	}

	public static void main(String[] args) {
		sayHello(args);
	}
}