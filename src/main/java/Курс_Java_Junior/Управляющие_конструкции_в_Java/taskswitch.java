package com.javacourse;

import java.util.Objects;

public class Main {
	public static void sayHello(String[] args) {
		// Получаем от системы имя текущего пользователя и название ОС
		String username = System.getProperty("user.name");
		String osName = System.getProperty("os.name");
		// Если в качестве аргументов передана не одна строка - "Hello!"
		if (args.length != 1) {
			System.out.println("Hello!");
			return;
		}
		// Иначе - сопоставляем параметр с искомыми и выводим соответствующие приветствия.
		// При этом приветствия для параметров "-us" и "-su" одинаковы
		switch (args[0]) {
			case "-u":
				System.out.println("Hello, " + username + "!");
				break;
			case "-s":
				System.out.println("Hello, " + osName + "!");
				break;
			case "-su":
			case "-us":
				System.out.println("Hello, " + username + " of "+ osName +"!");
				break;
			default:
				System.out.println("Hello!");
		}
	}

	public static void main(String[] args) {
		sayHello(args);
	}
}