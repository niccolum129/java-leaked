package com.javacourse;

import java.util.Objects;

public class Main {
	public static void sayHello(String[] args) {
		// Получаем от системы имя текущего пользователя и название ОС
		String username = System.getProperty("user.name");
		String osName = System.getProperty("os.name");
		// Если передан один аргумент, проверяем его на соответствие "-u" или "-s"
		if (args.length == 1 && Objects.equals(args[0], "-u"))
			System.out.println("Hello, " + username + "!");
		else if (args.length == 1 && Objects.equals(args[0], "-s"))
			System.out.println("Hello, " + osName + "!");
		// Если аргумента два, проверяем: должна быть пара "-u" и "-s"
		else if (args.length == 2 &&
			(Objects.equals(args[0], "-u") && Objects.equals(args[1], "-s")
			|| Objects.equals(args[0], "-s") && Objects.equals(args[1], "-u")))
			System.out.println("Hello, " + username + " of "+ osName +"!");
		// Иначе - "Hello!"
		else
			System.out.println("Hello!");
	}

	public static void main(String[] args) {
		sayHello(args);
	}
}