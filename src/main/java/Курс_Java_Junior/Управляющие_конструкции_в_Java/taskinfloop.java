package com.javacourse;

import java.util.Scanner;

public class WindowResolver {

    public static void main(String[] args){
        while (true) {
            int serviceNum = getUserInput();
            int targetWindow;
            switch (serviceNum) {
                case 1:
                case 4:
                case 7:
                    targetWindow = 26;
                    break;
                case 2:
                case 3:
                case 5:
                case 6:
                    targetWindow = 27;
                    break;
                case 8:
                case 9:
                case 10:
                    targetWindow = 28;
                    break;
                default:
                    targetWindow = 30;
                    break;
            }
            System.out.println("По вашему вопросу обратитесь в окно " + targetWindow);
        }
    }

    static Scanner in = new Scanner(System.in);
    private static int getUserInput(){
        return in.nextInt();
    }
}
