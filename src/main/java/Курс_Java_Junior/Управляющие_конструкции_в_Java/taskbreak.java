Пример 1:
for (Map.Entry<Integer, String> certEntry : certificates.entrySet()) {
	Integer certId = certEntry.getKey();
	String certJson = certEntry.getValue();

	Map<String, Object> cert = parseJson(certId, certJson);
	//Изменить нужно только строки кода, идущие ниже
	if (cert != null) {
		parseFieldNames(cert);
	}
}

Пример 2 (вариант №1):
for (ExpertResponse item : list) {
	BigDecimal total;
	if (item.getRang() != null)
		total = item.getRang().getTotal();
	if (total != null) {
		if (!map.containsKey(total)) {
			map.put(total, new ArrayList());
		}
		map.get(total).add(item);
	}
}

Пример 2 (вариант №2):
for (ExpertResponse item : list) {
	BigDecimal total;
	if (item.getRang() != null && total = item.getRang().getTotal() != null) {
		if (!map.containsKey(total)) {
			map.put(total, new ArrayList());
		}
		map.get(total).add(item);
	}
}