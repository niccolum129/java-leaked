package com.javacourse;

import java.util.Objects;

public class Main {
	public static void checkWeight(double[] weights) {
		// Проверяем массив на пустоту. Если таблеток нет, говорим
		if (weights.length == 0) {
			System.out.println("Not enough substance");
			return;
		}
		final double standardWeight = 0.050; // эталонный вес
		boolean isOk = true; // Есть ли бракованные таблетки? Пока считаем, что все таблетки имеют нужный вес
		for (var weight : weights)
			// Вычисляем процент, который составляет вес таблетки weight от standardWeight. Он должен быть более 90.
			// Если нет, печатаем вес таблетки и отмечаем, что есть бракованные таблетки
			if (weight / (standardWeight / 100) <= 90) {
				System.out.println(weight);
				isOk = false;
			}
		if (isOk)
			System.out.println("Ok");
	}

	public static void main(String[] args) {
		double[] tabletsWeights = {0.02, 0.048, 0.052, 0.037, 0.050, 0.048, 0.049, 0.054, 0.048, 0.045};
		checkWeight(tabletsWeights);
	}
}