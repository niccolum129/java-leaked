package com.javacourse;

import java.util.Objects;

public class Main {
	public static void checkWeight(double[] weights) {
		final double standardWeight = 0.050; // эталонный вес
		// Вычисляем средний вес
		int i = 0;
		double sumWeight = 0;
		int tabletsCount = weights.length;
		double midWeight = 0;
		// Если таблеток нет, ничего считать и не надо - средний вес нулевой
		if (tabletsCount != 0) {
			do {
				sumWeight += weights[i];
				i++;
			} while (i < tabletsCount);
			midWeight = sumWeight / tabletsCount;
		}
		// Вычисляем процент, который составляет средний вес midWeight от standardWeight. Он должен быть более 90
		String resultText = (midWeight / (standardWeight / 100)) > 90 ? "Ok" : "Not enough substance";
		System.out.println(resultText);
	}

	public static void main(String[] args) {
		double[] tabletsWeights = {0.02, 0.048, 0.052, 0.037, 0.050, 0.048, 0.049, 0.054, 0.048, 0.045};
		checkWeight(tabletsWeights);
	}
}