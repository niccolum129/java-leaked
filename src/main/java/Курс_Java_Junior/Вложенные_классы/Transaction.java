package Курс_Java_Junior.Вложенные_классы;

public class Transaction {
    private double price;
    private final double tax = 1.2;

    public Transaction() {
        this.price = 0;
    }

    public double getPrice() {
        return this.price;
    }

    public double dealPrice() {
        return this.price * this.tax;
    }

    private void printCheck(double price) {
        System.out.println(String.format("price: %.2f USD", price));
    }

    public void printCheck() {
        System.out.print("Order ");
        printCheck(this.price);
        System.out.print("Total ");
        printCheck(this.dealPrice());
    }

    public class TransactionItem extends ProductInfo {

        public TransactionItem(String name, double price) {
            this.name = "Default";
            if (name != null && !name.trim().isEmpty())
                this.name = name;

            this.price = Math.max(0, price);

            getTransaction().price += this.price;
        }

        public Transaction getTransaction() {
            return Transaction.this;
        }

        public void printInfo() {
            System.out.printf("Item: %s, ", this.name);
            getTransaction().printCheck(this.price);
        }
    }
}
