package Курс_Java_Junior.Вложенные_классы;

public class Receipt {
    public static void printReceipt(ProductInfo[] infos) {
        Transaction transcation = new Transaction();

        for (ProductInfo product : infos) {
            Transaction.TransactionItem item = transcation.new TransactionItem(product.name, product.price);
            item.printInfo();
        }
        transcation.printCheck();
    }

    public static void printTransactionInfo(Transaction.TransactionItem item) {
        item.getTransaction().printCheck();
    }
}
