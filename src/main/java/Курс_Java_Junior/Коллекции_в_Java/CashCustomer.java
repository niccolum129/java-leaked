package Курс_Java_Junior.Коллекции_в_Java;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CashCustomer extends Customer {
    private String documentNumber;

    public CashCustomer(String name, String documentNumber, int purchaseCount) {
        super(purchaseCount, name);
        this.documentNumber = "0000 000000";

        if (documentNumber != null) {
            Matcher matcher = Pattern
                    .compile("\\b\\d{4} \\d{6}\\b")
                    .matcher(documentNumber);
            if (matcher.matches()) {
                this.documentNumber = documentNumber;
            }
        }
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    @Override
    public void customerInfo() {
        // Customer <name> (passport: <documentNumber>) has a discount <discountSize>%<nl>
        System.out.printf("Customer %s (passport: %s) has a discount %.0f%% %c",
                this.getName(),
                this.getDocumentNumber(),
                this.getDiscountSize() * 100,
                TechArraySymbols.NEW_LINE_SYMBOL.getSymbol());
    }
}
