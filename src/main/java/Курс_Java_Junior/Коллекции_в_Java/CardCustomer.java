package Курс_Java_Junior.Коллекции_в_Java;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CardCustomer extends Customer {
    private String cardNumber;

    public CardCustomer(String name, String cardNumber, int purchaseCount) {
        super(purchaseCount, name);
        this.cardNumber = "0000000000000000";

        if (cardNumber != null) {
            Matcher matcher = Pattern
                    .compile("\\b\\d{16}\\b")
                    .matcher(cardNumber);
            if (matcher.matches()) {
                this.cardNumber = cardNumber;
            }
        }
    }

    public String getCardNumber() {
        return cardNumber;
    }

    @Override
    public void customerInfo() {
        // Customer <name> (card: <cardNumber>) has a discount <discountSize>%<nl>
        System.out.printf("Customer %s (card: %s) has a discount %.0f%%%c",
                this.getName(),
                this.getCardNumber(),
                this.getDiscountSize() * 100,
                TechArraySymbols.NEW_LINE_SYMBOL.getSymbol());
    }
}
