package Курс_Java_Junior.Коллекции_в_Java;

import java.util.Objects;

public class Consumer {
    private String fullName;
    private String shortName;
    private int country;
    private String uniqueId;

    public Consumer(String fullName, String shortName, int country, String uniqueId) {
        this.fullName = fullName;
        this.shortName = shortName;
        this.country = country;
        this.uniqueId = uniqueId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public int getCountry() {
        return country;
    }

    public void setCountry(int country) {
        if (country > 0 && country < 1000)
            this.country = country;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Consumer consumer = (Consumer) o;
        return country == consumer.country &&
                fullName.equals(consumer.fullName) &&
                shortName.equals(consumer.shortName) &&
                uniqueId.equals(consumer.uniqueId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fullName, shortName, country, uniqueId);
    }
}
