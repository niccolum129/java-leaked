package Курс_Java_Junior.Коллекции_в_Java;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Consumers {
    private final Set<Consumer> consumerSet;

    public Consumers() {
        consumerSet = new HashSet<>();
    }

    public Consumers(List<Consumer>[] consumersArray) {
        this();
        for (List<Consumer> consumerList : consumersArray) {
            consumerSet.addAll(consumerList);
        }
    }

    public void addConsumer(Consumer consumer) {
        consumerSet.add(consumer);
    }

    public List<Consumer> getConsumers() {
        return new ArrayList<>(consumerSet);
    }
}