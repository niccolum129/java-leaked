package Курс_Java_Junior.Коллекции_в_Java;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Shop {
    private List<Customer> shopCustomers;

    private Shop(List<Customer> shopCustomers) {
        this.shopCustomers = shopCustomers;
    }

    public void printShopSummary() {
        for (Customer customer : this.shopCustomers) {
            customer.customerInfo();
        }
    }

    public static Shop createShopInfo() {
        Scanner scanner = new Scanner(System.in);
        List<Customer> customerList = new ArrayList<>();
        String input = "";
        Pattern pattern = Pattern.compile("(\\d),([a-zA-Z ]*),(\\d+),(\\d{4} \\d{6})?,(\\d{16})?");

        System.out.println("Shop data:");
        while (!input.equals("exit")) {
            input = scanner.nextLine();
            Matcher matcher = pattern.matcher(input);

            addCustomer(customerList, matcher);
        }

        return new Shop(customerList);
    }

    private static void addCustomer(List<Customer> customerList, Matcher matcher) {
        if (matcher.find()) {
            int type = Integer.parseInt(matcher.group(1));
            String name = matcher.group(2);
            int purchaseCount = Integer.parseInt(matcher.group(3));
            String special;

            if (type == 1) {
                special = matcher.group(4);
                customerList.add(new CashCustomer(name, special, purchaseCount));
            } else {
                special = matcher.group(5);
                customerList.add(new CardCustomer(name, special, purchaseCount));
            }
        }
    }
}
