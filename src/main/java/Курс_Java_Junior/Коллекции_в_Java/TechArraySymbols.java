package Курс_Java_Junior.Коллекции_в_Java;

public enum TechArraySymbols {
    NEW_LINE_SYMBOL('\n');

    private final char symbol;

    TechArraySymbols(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }
}
