package Курс_Java_Junior.Коллекции_в_Java;

public abstract class Customer {
    private int purchaseCount;
    private double discountSize;
    private String name;

    public Customer(int purchaseCount, String name) {
        this.setPurchaseCount(purchaseCount);

        this.name = "No-name";
        if (name != null && !name.trim().isEmpty()) {
            this.name = name;
        }
    }

    public int getPurchaseCount() {
        return purchaseCount;
    }

    public void setPurchaseCount(int purchaseCount) {
        if (purchaseCount < 0) {
            // TODO: обработка некорректных данных?
            return;
        }
        this.purchaseCount = purchaseCount;

        if (purchaseCount < 5) {
            this.discountSize = 0;
        }
        else if (purchaseCount < 10) {
            this.discountSize = 0.05;
        }
        else if (purchaseCount < 15) {
            this.discountSize = 0.1;
        }
        else {
            this.discountSize = 0.2;
        }
    }

    public void incrementPurchaseCount() {
        this.setPurchaseCount(
                this.getPurchaseCount() + 1
        );
    }

    public double getDiscountSize() {
        return discountSize;
    }

    public String getName() {
        return name;
    }

    public abstract void customerInfo();
}
