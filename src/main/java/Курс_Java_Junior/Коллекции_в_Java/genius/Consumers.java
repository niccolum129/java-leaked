package Курс_Java_Junior.Коллекции_в_Java.genius;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

// Класс для хранения информации обо всех контрагентах
public class Consumers {
    // ----Поля
    private Set<Consumer> consumersSet;

    // ----Конструкторы
    // Конструктор по умолчанию
    public Consumers() {
        // Создаём пустое множество для хранения контрагентов
        consumersSet = new HashSet<Consumer>();
    }

    // Конструктор, инициализирующий объект информацией о контрагентах
    public Consumers(List<Consumer>[] consumersLists) {
        // Вызываем конструктор по умолчанию для создания пустого множества
        this();
        // Заполняем множество контрагентами
        for (var consumersList : consumersLists)
            for (var consumer : consumersList)
                consumersSet.add(consumer);
    }

    // ----Методы
    // Добавить в объект информацию о контрагенте
    public void addConsumer(Consumer consumer) {
        // Добавляем контрагента во множество
        consumersSet.add(consumer);
    }

    // Получить список уникальных контрагентов
    public List<Consumer> getConsumers() {
        return new ArrayList<Consumer>(consumersSet);
    }
}
