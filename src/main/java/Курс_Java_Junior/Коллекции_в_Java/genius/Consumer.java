package Курс_Java_Junior.Коллекции_в_Java.genius;

import java.util.Objects;

// Класс контрагента
public class Consumer {
    // ----Поля
    // Полное наименование контрагента
    private String fullName;
    public String getFullName() { return fullName; }
    public void setFullName(String fullName) { this.fullName = fullName; }

    // Сокращенное наименование
    private String shortName;
    public String getShortName() { return shortName; }
    public void setShortName(String shortName) { this.shortName = shortName; }

    // Числовой код страны регистрации контрагента, в соответствии со стандартом ISO 3166-1
    private int country;
    public int getCountry() { return country; }
    public void setCountry(int country) { this.country = country; }

    // Уникальный идентификационный номер контрагента по месту регистрации
    private String uniqueId;
    public String getUniqueId() { return uniqueId; }
    public void setUniqueId(String uniqueId) { this.uniqueId = uniqueId; }

    // ----Конструкторы
    // Конструктор по умолчанию
    public Consumer() {
        fullName = "DefaultFullName";
        shortName = "DefaultShortName";
        country = 0;
        uniqueId = "DefaultUniqueId";
    }

    // Конструктор, инициализирующий все поля соответствующими значениями
    public Consumer(String fullName, String shortName, int country, String uniqueId) {
        // Проверяем полное наименование
        if (fullName == null || fullName.length() == 0)
            System.out.println("Warning! Full name is empty!");
        this.fullName = fullName;
        // Проверяем сокращённое наименование
        if (shortName == null || shortName.length() == 0)
            System.out.println("Warning! Short name is empty!");
        this.shortName = shortName;
        // Проверяем страну. Коды находятся в диапазоне от 004 до 894 (стандарт ISO 3166-1)
        if (country < 0 || country > 900)
            System.out.println("Warning! Country code is out of correct codes range!");
        this.country = country;
        // Проверяем уникальный идентификатор
        if (uniqueId == null || uniqueId.length() == 0)
            System.out.println("Warning! Unique id is empty!");
        this.uniqueId = uniqueId;
    }

    // ----Для того, чтобы Set мог сравнивать объекты класса Consumer на равенство, необходимо переопределить
    // методы equals и hashCode класса Object
    @Override
    public boolean equals(Object o) {
        // Если сравниваем один и тот же объект, то объекты равны
        if (this == o) return true;
        // Если объекта для сравнения не существует или он другого класса - объекты разные
        if (o == null || getClass() != o.getClass())
            return false;
        // Считаем, что два объекта Consumer являются равными (одинаковыми), если равны их страны регистрации
        // и уникальные номера
        Consumer consumer = (Consumer) o;
        return country == consumer.country && Objects.equals(uniqueId, consumer.uniqueId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(country, uniqueId);
    }
}
