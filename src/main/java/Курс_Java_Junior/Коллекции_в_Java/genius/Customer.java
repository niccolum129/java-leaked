package Курс_Java_Junior.Коллекции_в_Java.genius;

import java.util.HashMap;
import java.util.Map;

// Класс клиента
public class Customer {
    // Идентификатор
    private int id;

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

    // Имя
    private String name;

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    // Номер телефона
    private String phone;

    public String getPhone() { return phone; }
    public void setPhone(String phone) { this.phone = phone; }

    // Сумма покупок клиента
    private double purchaseCount;

    public double getPurchaseCount() { return purchaseCount; }
    public void setPurchaseCount(double purchaseCount) { this.purchaseCount = purchaseCount; }

    // Статическое поле для хранения данных, загруженных через метод load.
    // Информация о клиентах хранится в хеш-таблице: ключ - идентификатор, значение - объект клиента
    private static Map<Integer, Customer> customersMap = null;

    // Конструктор
    public Customer(int id, String name, String phone, double purchaseCount) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.purchaseCount = purchaseCount;
    }


    // Методы

    // Загрузить массив клиентов из БД для дальнейшей работы
    public static void load(Customer[] customers) {
        // Если был передан null вместо массива, или массив был пуст, обнуляем хеш-таблицу и завершаем загрузку
        if (customers == null || customers.length == 0) {
            customersMap = null;
            return;
        }
        // Создаём новую хеш-таблицу для хранения клиентов из БД
        customersMap = new HashMap<Integer, Customer>();
        // Заполняем хеш-таблицу парами "идентификатор клиента - объект клиента". На всякий случай проверяем на null
        for (var customer : customers)
            if (customer != null)
                customersMap.put(customer.id, customer);
    }

    // Получить клиента по значению его идентификатора
    public static Customer getById(int id) {
        // Если БД отсутствует (или была загружена пустая БД) - null
        if (customersMap == null)
            return null;
        // Возвращаем из хеш-таблицы нужного клиента. Если такого нет - null
        return customersMap.containsKey(id) ? customersMap.get(id) : null;
    }
}