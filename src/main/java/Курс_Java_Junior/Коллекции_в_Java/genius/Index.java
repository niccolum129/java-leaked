package Курс_Java_Junior.Коллекции_в_Java.genius;

import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Index {
    // ----Поля
    // Частотная таблица текста, сортирующая слова-ключи в алфавитном порядке
    private SortedMap<String, Integer> frequencyTable;
    public SortedMap<String, Integer> getFrequencyTable() { return frequencyTable; }

    // Паттерн для регулярного выражения, с помощью которого мы разбиваем текст на слова
    private static final Pattern wordPattern = Pattern.compile("\\w+",
            Pattern.UNICODE_CHARACTER_CLASS | Pattern.CASE_INSENSITIVE);

    // ----Конструкторы
    // Конструктор по умолчанию, инициализирующий частотную таблицу
    public Index() {
    // Создаём частотный словарь. По умолчанию ключи будут сортироваться в алфавитном порядке
        frequencyTable = new TreeMap<String, Integer>();
    }

    // Конструктор, инициализирующий частотную таблицу для заданного текста
    public Index(String text) {
        // Вызываем конструктор без параметров
        this();
        // Вызываем частотный анализ - наполняем таблицу частот
        doFrequencyAnalysis(text);
    }

    // ----Методы
    // Дополнить таблицу частот частотами из заданного текста
    private void doFrequencyAnalysis(String text) {
        // Если текст отсутствует - прерываем анализ
        if (text == null)
            return;
        // Приводим текст к нижнему регистру
        var lowerText = text.toLowerCase();
        // Делим строку на слова, используя регулярное выражение. Заполняем таблицу частот
        Matcher matcher = wordPattern.matcher(lowerText);
        // Пока в тексте находятся слова
        while (matcher.find()) {
            // Получаем очередное слово
            var word = matcher.group();
            // Если это слово уже содержится в таблице, увеличиваем его частоту на 1
            if (frequencyTable.containsKey(word)) {
                int prevFreq = frequencyTable.get(word);
                frequencyTable.put(word, prevFreq + 1);
            }
            // Иначе - ставим ему единичную частоту
            else
                frequencyTable.put(word, 1);
        }
    }

    // Получить частоту встречаемости слова в тексте
    public int getWordFrequency(String word) {
        // Если слово отсутствует, возвращаем 0
        if (word == null)
            return 0;
        // Если это слово есть в словаре - возвращаем его частоту. Если нет - нуль
        return frequencyTable.containsKey(word) ? frequencyTable.get(word) : 0;
    }

    // Добавить к анализируемому тексту заданный текст
    public void appendText(String text) {
        // Вызываем частотный анализ - дополняем таблицу частот
        doFrequencyAnalysis(text);
    }
}