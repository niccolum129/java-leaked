package Курс_Java_Junior.Работа_с_базовыми_типами_данных_как_с_классами;

package com.intellekta.pinapple;

public class ClientClassifier {
    public static void getClientsByType(Client[] clients, String targetType, Client[] result) {
        if (clients == null || clients.length == 0) {
            System.out.println("Clients array is null or empty");
            return;
        }
        if (result == null || result.length == 0) {
            System.out.println("Result array is null or empty");
            return;
        }

        int actualSize = 0;
        for (Client client : clients) {
            if (client.type.intern() == targetType.intern()) {
                if (actualSize >= result.length) {
                    System.out.println("Result array length exceeded");
                    return;
                }
                result[actualSize++] = client;
            }
        }
    }

    public static void printClients(Client[] clients) {
        if (clients == null)
            return;
        for (Client client : clients) {
            System.out.printf("{%d};{%s};{%s};{%f}%n",
                    client.id,
                    client.name,
                    client.type,
                    client.sum);
        }
    }
}
