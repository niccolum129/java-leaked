package Курс_Java_Junior.Работа_с_базовыми_типами_данных_как_с_классами;

package com.intellekta.pinapple;

public class Client {
    long id;
    String type;
    String name;
    double sum;

    public Client(long id, String type, String name, double sum) {
        this.id = Math.max(id, 100_000_000_000L);

        this.type = "institution";
        if ("individual".equals(type))
            this.type = type;

        this.name = "Default";
        if (name != null)
            this.name = name;

        this.sum = Math.max(0, sum);
    }

    public String getType() {
        return type;
    }
}