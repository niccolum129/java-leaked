package Курс_Java_Junior.Работа_с_базовыми_типами_данных_как_с_классами;

public class SortingUtil {
    // по возрастанию
    public static void sortInts(int[] data) {
        // проверка на инициализацию мпссива и на его пустоту
        if (data == null || data.length == 0) {
            System.out.println("Empty Data");
            return;
        }
        // если все хорошо, сортируем:
        bubbleSort(data);
    }

    // по возрастанию, null в конец
    public static void sortDoubles(Double[] data) {
        // проверка на инициализацию мпссива и на его пустоту
        if (data == null || data.length == 0) {
            System.out.println("Empty Data");
            return;
        }
        // если все хорошо, сортируем:
        bubbleSort(data);
    }

    // сортировка пузырьком по возрастанию примитивного int
    private static void bubbleSort(int[] data) {
        for (int i = 1; i < data.length; i++) {
            for (int j = 0; j < data.length - i; j++) {
                if (data[j] > data[j + 1]) {
                    swap(data, j, j + 1);
                }
            }
        }
    }

    // сортировка пузырьком по возрастанию Double, null перемещаем в конец
    private static void bubbleSort(Double[] data) {
        for (int i = 1; i < data.length; i++) {
            for (int j = 0; j < data.length - i; j++) {
                // чтобы переместить null в конец, получим значение в ячейке массива,
                // если null, подставляем вместо него максимальное возможное значение
                double v1 = data[j] != null ? data[j] : Double.MAX_VALUE;
                double v2 = data[j + 1] != null ? data[j + 1] : Double.MAX_VALUE;

                // сравниваем примитивные double, но переставляем обертки
                if (v1 > v2) {
                    swap(data, j, j + 1);
                }
            }
        }
    }

    // метод для перестановки значений в массиве примитивных int
    private static void swap(int[] data, int idx1, int idx2) {
        // сохраняем значения под idx1 во временные переменные
        int tmp = data[idx1];
        // меняем элементы:
        data[idx1] = data[idx2];
        data[idx2] = tmp;
    }

    // метод для перестановки значений в массиве Double
    private static void swap(Double[] data, int idx1, int idx2) {
        // сохраняем значения под idx1 во временные переменные
        Double tmp = data[idx1];
        // меняем элементы:
        data[idx1] = data[idx2];
        data[idx2] = tmp;
    }
}
