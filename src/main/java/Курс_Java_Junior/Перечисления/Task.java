package Курс_Java_Junior.Перечисления;

public class Task {
    enum Planes {
        BOEING(8000, 10),
        IL90(5400, 9),
        TU153(14000, 15);

        public int distance;
        public int time;

        Planes(int distance, int time) {
            this.distance = distance;
            this.time = time;
        }

        public int speed() {
            return distance / time;
        }
    }

    public int maxSpeed() {
        int maxSpeed = -1;
        for (Planes plane : Planes.values()) {
            int curPlaneSpeed = plane.speed();
            if (curPlaneSpeed > maxSpeed) {
                maxSpeed = curPlaneSpeed;
            }
        }

        return maxSpeed;
    }
}
